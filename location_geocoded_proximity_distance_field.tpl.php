<?php

drupal_add_js(drupal_get_path('module', 'location_geocoded_proximity') . '/location_geocoded_proximity.js');

$outputId = $element['#id'] . '-output';

$dataForJavasript = array();
$dataForJavascript['locationGeocodedProximityUpdateRange']['id'] = $element['#id'];
$dataForJavascript['locationGeocodedProximityUpdateRange']['outputid'] = $outputId;

drupal_add_js($dataForJavascript, 'setting');

$output = "";
if (isset($element['#field_prefix'])) {
  $output .= '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ';
}
$output .= '<input type="range" name="' . $element['#name'] 
        . '" id="' . $element['#id'] . '"' 
        . $size . ' value="' . check_plain($element['#value']) . '"' 
        . drupal_attributes($element['#attributes']) 
        . ' />';

$output .= '<output id="' . $outputId . '"  name="distanceoutput" for="' . $element['#name'] . '"></output>Km';
if (isset($element['#field_suffix'])) {
  $output .= ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>';
}

print (theme('form_element', $element, $output));
