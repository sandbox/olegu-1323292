<?php

class location_geocoded_proximity_handler_field_distance extends views_handler_field {
  
  function click_sort($order) { 
    $coordinates = $this->get_coordinates_from_filter();

    if(!empty($coordinates)) 
      $this->query->add_orderby(NULL, earth_distance_sql($coordinates->lng, $coordinates->lat, $this->table_alias), $order, $this->field_alias);
  }

  function query() {
    $this->ensure_my_table();

    $coordinates = $this->get_coordinates_from_filter();
    if (empty($coordinates)) {
      $this->field_alias = $this->query->add_field(NULL, "'Unknown'", $this->table_alias .'_'. $this->field); 
      return;
    }

    $this->field_alias = $this->query->add_field(NULL, earth_distance_sql($coordinates->lng, $coordinates->lat, $this->table_alias), $this->table_alias .'_'. $this->field);
  }

  function render($values) {
    if (empty($values->{$this->field_alias}) || $values->{$this->field_alias} == 'Unknown') {
        return;
    }
       
    $distance = (float)$values->{$this->field_alias};
    $distance = $distance / 1000.0;
    $distance_string = number_format($distance, 1);
    return $distance_string;
  }

  private function get_coordinates_from_filter() {
    return $this->view->filter['geocoded_distance']->options['retrieved_coordinates'];
  }
}
