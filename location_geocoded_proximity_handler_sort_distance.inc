<?php

class location_geocoded_proximity_handler_sort_distance extends views_handler_sort {

  function query() {
    $this->ensure_my_table();
  
    $coordinates = $this->get_coordinates_from_filter();
    if (empty($coordinates))
      return;

    $alias = $this->table_alias . '_' . $this->field . '_sort';

    $this->query->add_orderby(NULL, earth_distance_sql($coordinates->lng, $coordinates->lat, $this->table_alias), $this->options['order'], $alias);
    
  }

  private function get_coordinates_from_filter() {
    return $this->view->filter['geocoded_distance']->options['retrieved_coordinates'];
  }
}
