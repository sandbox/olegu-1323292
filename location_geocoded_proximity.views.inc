<?php
  function location_geocoded_proximity_views_handlers() {
    return array(
      'info' => array(
        'path' => drupal_get_path('module', 'location_geocoded_proximity')
      ),
      'handlers' => array (
        'location_geocoded_proximity_handler_filter_proximity' => array(
          'parent' => 'views_handler_filter'
        ),
        'location_geocoded_proximity_handler_field_distance' => array(
          'parent' => 'views_handler_field'
        ),
        'location_geocoded_proximity_handler_sort_distance' => array(
          'parent' => 'views_handler_sort'
        )
      )
    );
  }

  function location_geocoded_proximity_views_data() {
    $data['location']['geocoded_distance'] = array(
      'title' => t('Geocoded distance / Proximity'),
      'help' => t("The distance from a location retrieved by geocoding using the google maps webservices"),
      'field' => array(
        'handler' => 'location_geocoded_proximity_handler_field_distance',
        'click sortable' => TRUE
      ),
      'sort' => array(
        'handler' => 'location_geocoded_proximity_handler_sort_distance'
      ),
      'filter' => array(
        'handler' => 'location_geocoded_proximity_handler_filter_proximity',
      )
    );
    return $data;
  }

  

