Drupal.behaviors.locationGeocodedProximityUpdateRange = function (context) {
  rangeId = '#' + Drupal.settings.locationGeocodedProximityUpdateRange.id;
  outputId = '#' + Drupal.settings.locationGeocodedProximityUpdateRange.outputid;
  if ($(rangeId).attr('type').toUpperCase() == 'RANGE') {
    $(outputId).val($(rangeId).val());  
    $(rangeId).change(function() {
     $(outputId).val($(rangeId).val());
    });
  }
}

