<?php

class location_geocoded_proximity_handler_filter_proximity extends views_handler_filter { 
  var $no_single = TRUE; 
  function option_definition() {
    $options = parent::option_definition();

    $options['operator'] = array('default' => 'mbr');

    $options['identifier'] = array('default' => 'geodist');

    $options['value'] = array(
      'default' => array(
        'latitude' => '',
        'longitude' => '',
        'location_name' => '',
        'search_distance' => 10
      )
    );
    $options['region_bias'] = array('default' => '');
    $options['log_geocoding_errors'] = array('default' => FALSE);
    $options['geocoding_error_message'] = array ('default' => t('Location coordinates could not be found'));
    $options['retrieved_coordinates'] = array ('default' => null);
    $options['expose']['hide'] = array('default' => FALSE);
    $options['expose']['location_name_title'] = array('default' => t('Location name'));
    $options['expose']['location_name_description'] = array('default' => t('Enter the name of a location to search relative to'));
    $options['expose']['search_distance_title'] = array('default' => t('Distance (Km)'));
    $options['expose']['search_distance_description'] = array('default' => t('The furthest distance from the given location to include'));
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form,$form_state);
    $form['region_bias'] = array(
      '#type' => 'textfield',
      '#title' => t('Region bias'),
      '#default_value' => $this->options['region_bias'],
      '#description' => t('Google geocoding api region bias, (country code top-level domain)')
    );

    $form['log_geocoding_errors'] = array(
      '#type' => 'checkbox',
      '#title' => t('Log invalid returns from the google geocoding service'),
      '#default_value' => $this->options['log_geocoding_errors'],
      '#description' => t('Enable logging of non valid returns from the google geocoding service.  Records returned status code, and location name searched for')
    );

    $form['geocoding_error_message'] = array(
      '#type' => 'textfield',
      '#title' => t('Failed Geocoding error message'),
      '#default_value' => $this->options['geocoding_error_message'],
      '#description' => t('Friendly error message to show the user when geocoding fails. The same message is shown for communication failure, json decode error and simply no results.')
    );
  }

  function expose_form_left(&$form, &$form_state) {
    parent::expose_form_left($form,$form_state);
    $form['expose']['hide'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide exposed controls for the proximity filter'),
      '#default_value' => $this->options['expose']['hide'],
      '#description' => t('Hides the exposed controls, usefule for multistep filtering')
    );
  }


  function expose_form_right(&$form, &$form_state) {
    parent::expose_form_right($form, $form_state);
    $form['expose']['location_name_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title to present for the location name field'),
      '#default_value' => $this->options['expose']['location_name_title']
    );

    $form['expose']['location_name_description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description for the location name field'),
      '#default_value' => $this->options['expose']['location_name_description']
    );

    $form['expose']['search_distance_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title to present for the search distance field'),
      '#default_value' => $this->options['expose']['search_distance_title']
    );

    $form['expose']['search_distance_description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description for the search distance field'),
      '#default_value' => $this->options['expose']['search_distance_description']
    );
      

  }

  function admin_summary() { return "";}

  function operator_options() {
    return array (
      'mbr' => t('Proximity (Rectangular)'),
      'dist' => t('Proximity (Circular)')
    );
  }

  function value_form(&$form, &$form_state) {
    //hack liftet from original location_views_handler_filter_proximity.inc
    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
      if (!isset($form_state['input'][$identifier])) {
        // We need to pretend the user already inputted the defaults, because
        // fapi will malfunction otherwise.
        $form_state['input'][$identifier] = $this->value;
      }
    }
    $form['value'] = array(
      '#tree' => TRUE
    );
    $form['value']['location_name'] = array(
      '#type' => 'textfield',
      '#title' => $this->options['expose']['location_name_title'],
      '#default_value' => $this->value['location_name'],
      '#description' => $this->options['expose']['location_name_description'],
    );
    $form['value']['search_distance'] = array(
      '#type' => 'textfield',
      '#title' => $this->options['expose']['search_distance_title'],
      '#theme' => 'location_geocoded_proximity_distance_field',
      '#default_value' => $this->value['search_distance'],
      '#description' => $this->options['expose']['search_distance_description'],
    );

  }

  function exposed_form(&$form, &$form_state) {
    parent::exposed_form($form, $form_state);
    $key = $this->options['expose']['identifier'];
    if ($this->options['expose']['hide']) {
      $form[$key]['location_name']['#type'] = 'hidden';
      $form[$key]['search_distance']['#type'] = 'hidden';
      unset($form[$key]['search_distance']['#theme']);
    }
  }

  function query() {
    if (empty($this->value) || empty($this->value['location_name'])) 
       return;
    
    $this->ensure_my_table();
    
    $geocoding_response = $this->get_geocoding_response($this->value['location_name']);
    if (!$this->response_is_valid($geocoding_response)) {
      drupal_set_message($this->options['geocoding_error_message'], 'warning');
      return;
    }
    
    $coordinates = $this->get_coordinates_from_response($geocoding_response);
    //making the coordinates accessible for sort and field
    $this->options['retrieved_coordinates'] = $coordinates;
    
    $distance = $this->get_distance_in_meters($this->value['search_distance']);

    $latrange = earth_latitude_range($coordinates->lng, $coordinates->lat, $distance);
    $lonrange = earth_longitude_range($coordinates->lng, $coordinates->lat, $distance);
    $where = $this->build_condition($latrange, $longrange);
    
    $this->query->add_where($this->options['group'], $where, $latrange[0], $latrange[1], $lonrange[0], $lonrange[1]);

    if ($this->operator == 'dist') {
      $this->query->add_where($this->options['group'], earth_distance_sql($geodata->lng, $geodata->lat, $this->table_alias) .' < %f', $distance_meters);
    }
  }

  private function get_coordinates_from_response($response) {
    return $response->results[0]->geometry->location;
  }

  private function get_distance_in_meters($distance) {
    $distance = trim($distance);
    if (empty($distance)) 
      return 1000;

    return $distance * 1000;
  }


  private function build_condition($latrange, $longrange) {
    if ($lonrange[0] > $lonrange[1]) {
      $where = "$this->table_alias.latitude > %f " .
               "AND $this->table_alias.latitude < %f " .
               "AND (($this->table_alias.longitude < " .
               "AND $this->table_alias.longitude > %f) " .
               "OR ($this->table_alias.longitude < %f " .
               "AND $this->table_alias.longitude > -))";
    }
    else {
      $where = "$this->table_alias.latitude > %f " . 
               "AND $this->table_alias.latitude < %f  " . 
               "AND $this->table_alias.longitude > %f " . 
               "AND $this->table_alias.longitude < %f";
    }
    return $where;
  }

  private function response_is_valid($response) {
    if (empty($response)) 
      return false;
    
    if (empty($response->status)) {
      $this->log_geocoding_error(t('Response missing status field'));
      return false;
    }

    if (strtoupper($response->status) != "OK") {
      $this->log_geocoding_error($response->status);
      return false;
    }

    if (empty($response->results[0]->geometry->location)) {
      $this->log_geocoding_error(t('Response missing location data. Response: @response', array('@response' => print_r($response,TRUE))));
      return false;
    }

    return true;

  }

  private function log_geocoding_error($status) {
    $location_name = trim($this->value['location_name']);
    watchdog("Geocode proximity", "Status: $status, Location name: $location_name"); 
  }

  private function get_geocoding_response($location_name) {
    $url = $this->build_geocode_request_url($location_name);
    $json_response = file_get_contents($url);
    if ($json_response === FALSE) {
      $this->log_geocoding_error(t('Could not read result from host'));
      return null;
    }
    
    return $this->decode_geocoding_response($json_response);
  }

  private function decode_geocoding_response($json_response) {
    $response = json_decode($json_response);
    if (empty($response)) {
      $this->log_geocoding_error(t('Could not decode json: @json', array('@json' => $json_response)));
      return null;
    }

    return $response;
  }

  private function build_geocode_request_url($location_name) {
    $address = rawurlencode(trim($location_name));
    $region_bias = $this->create_region_bias_parameter();
    return "http://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false$region_bias";    
  }

  private function create_region_bias_parameter() {
    $rawvalue = trim($this->options['region_bias']);
    if (!empty($rawvalue))
      return "&region=" . rawurlencode($rawvalue);
    
    return '';
  }
}
